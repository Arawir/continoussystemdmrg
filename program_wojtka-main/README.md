# Program_Wojtka

# Compilation
    g++ main.cpp -o job -lgsl -Ofast -Wall -pedantic -std=c++17

# Run

./job n pmax ptot neig m corrPoints potentialType  g gdd σ - for dipole (or dipoleMy) potential
./job n pmax ptot neig m corrPoints potentialType  g vqw aqv - for WellV

# Przykładowe uruchomienie

./job 5 30 0 1 250 10 dipole  180 180 0.05

lub w dawnym programie Wojtka 5 30 0 0.45 -0.15 0.05 0. 1 250

  Preparing list of nonzero elements:  18095 / 18095 (DONE 5.10 [s], 30336 [kB])
  Generate Hamiltonian Matrix  (DONE 0.32 [s], 91840 [kB])
  LANCZOS: memory allocation  2 / 2 (DONE 0.00 [s], 93540 [kB])
  LANCZOS: generate matrix V:  10 / 10 (DONE 0.06 [s], 93680 [kB])
  Prepare hamiltonian matrix with offset (DONE 0.01 [s], 93680 [kB])
  LANCZOS: memory allocation  2 / 2 (DONE 0.00 [s], 129024 [kB])
  LANCZOS: generate matrix V:  250 / 250 (DONE 8.32 [s], 129024 [kB])
  State-> 249  Energy-> -1541.17  Energy variation (should be 0)-> -5.48363e-05  (DONE 0.05 [s], 32176 [kB])
  Calculating G2:  11 / 11 (DONE 6.11 [s], 163956 [kB])
  0	14.9841
  0.05	74.0335
  0.1	56.3301
  0.15	34.0311
  0.2	15.2735
  0.25	4.41237
  0.3	0.796328
  0.35	0.101849
  0.4	0.0139758
  0.45	0.00559031
  0.5	0.00483181
Everytihng done in 19.98 [s], peak vMemory 163956 [kB])


