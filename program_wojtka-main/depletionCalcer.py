import os
import sys
import numpy as np

N = int(sys.argv[1])
gamma = float(sys.argv[2])
sigma = float(sys.argv[3])
pMax = 50
pTot = 0
nEig = 1
m = 250
corrPoints = 1 
g= gamma*N


data = np.loadtxt("./data/energymap_n%i_pmax%i_ptot%i_neig%i_m%i_gamma%.6f_sigma%.6f.txt"%(N,pMax,pTot,nEig,m,gamma,sigma))
gammaDDTrans = data[-1,0]

#find max gdd
gdds = np.linspace(0.0,gammaDDTrans*N,100)
for gdd in gdds:
	os.system("./programWojtka %i %i %i %i %i %i dipole %.6f %.6f %.6f"%(N,pMax,pTot, nEig, m, corrPoints, g,gdd,sigma))


