import os
import sys

N = int(sys.argv[1])
pMax = int(sys.argv[2])
pTot = int(sys.argv[3])
nEig = float(sys.argv[4])
m =
corrPoints = 

if sys.argv[5] == 'infty':
	lamb='infty'
else:
	lamb = float(sys.argv[5])
lambdd = float(sys.argv[6])
sigma = float(sys.argv[7])
maxRange = float(sys.argv[8])

pMax
print("Ns=",Ns,", Nn=",Nn,", PBC=",PBC,", omega=",omega,", lambda=",lamb,", lambdd=",lambdd,", sigma=",sigma,", maxRange=",maxRange)

for N in range(2,10):
	if lamb=='infty':
		maxOcc=1
		g=0
		print('N=',N,'g=infty')
	else: 
		maxOcc=min(N,3)
		g=round(lamb*N,5)
		gdd=round(lambdd*N,5)
		print('N=',N,'g=',g,'gdd=',gdd)
	
	os.system("./programWojtka %i %i %i %i %i %i dipole %f %f %f"%(N,pMax,pTot, nEig, m, corrPoints, g,gdd,sigma))
	
	#./job 4 30 3 1 0 1 -2.0 0.033 8 3 > 4_30_3_1_0.000000_1.000000_-2.000000_0.033000_8_3.out

