import os
import sys
import numpy as np


def getEnergy(N,pMax,pTot,nEig,m,corrPoints,g,gdd,sigma):
	os.system("./programWojtka %i %i %i %i %i %i dipole %.6f %.6f %.6f"%(N,pMax,pTot, nEig, m, corrPoints, g,gdd,sigma))
	return np.loadtxt("./data/energies_n%i_pmax%i_ptot%i_neig%i_m%i_g%.6f_gdd%.6f_sigma%.6f.txt"%(N,pMax,pTot,nEig,m,g,gdd,sigma))[0]
	

N = int(sys.argv[1])
gamma = float(sys.argv[2])
sigma = float(sys.argv[3])
pMax = 50
pTot = 0
nEig = 1
m = 250
corrPoints = 2 
g= gamma*N

print("N=",N,", gamma=",gamma,", sigma=",sigma,", pMax=",pMax,", pTot=",pTot,", nEig=",nEig,", m=",m,", corrPoints=",corrPoints)

lgdd = 0*g
rgdd = 1.5*g

fileOut = open("./data/energymap_n%i_pmax%i_ptot%i_neig%i_m%i_gamma%.6f_sigma%.6f.txt"%(N,pMax,pTot,nEig,m,gamma,sigma),'w')

lE = getEnergy(N,pMax,pTot,nEig,m,corrPoints,g,lgdd,sigma)
rE = getEnergy(N,pMax,pTot,nEig,m,corrPoints,g,rgdd,sigma)

fileOut.write("%.12f %.12f\n"%(lgdd/N,lE))
fileOut.write("%.12f %.12f\n"%(rgdd/N,rE))	

if lE*rE >=0:
	print("ERROR 1")
else:
	for i in range(20):
		ngdd = (lgdd+rgdd)/2
		nE = getEnergy(N,pMax,pTot,nEig,m,corrPoints,g,ngdd,sigma)	
		fileOut.write("%.12f %.12f\n"%(ngdd/N,nE))
		if nE < 0:
			rgdd=ngdd
			rE=nE
		else:
			lgdd = ngdd
			lE=nE
			
fileOut.close()

