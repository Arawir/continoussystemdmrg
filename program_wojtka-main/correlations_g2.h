#ifndef CORRELATIONS_G2_H
#define CORRELATIONS_G2_H

#include "commonMethods.h"

void generateCovarianceMatrix(const Environement &env, const std::vector<BaseState> &B,  gsl_spmatrix * &covmatrix, double xp, const std::vector<Elem> &offDiagonalNonzeroElems){
    for(uint i=0; i<env.numberOfStates; i++){  //diagonal elements
        double element = 0.0;
        for(uint k=0; k<env.oneParticleStates; k++){
            element+=B[i][k]*(B[i][k]-1);
            for(uint j=0; j<k; j++){
                element+=2.0*B[i][k]*B[i][j]*(1.0+cos(xp*(k-j)*twoPI));
            }
        }
        gsl_spmatrix_set(covmatrix, i, i, element);
    }

    for(auto &elem : offDiagonalNonzeroElems){  //non-diagonal elements
        auto [permutationCoef, a0,a1,ad0,ad1] = getAsandAdds(elem.i, elem.ip, B);

        if( a0==a1 && ad0==ad1 ){
            gsl_spmatrix_set(covmatrix, elem.i, elem.ip, 2.0*permutationCoef);
        } else if( a0==a1 && ad0!=ad1 ){
            gsl_spmatrix_set(covmatrix, elem.i, elem.ip, 4.0*permutationCoef*cos(xp*(a0-ad0)*twoPI));
        } else if( a0!=a1 && ad0==ad1 ){
            gsl_spmatrix_set(covmatrix, elem.i, elem.ip, 4.0*permutationCoef*cos(xp*(a0-ad0)*twoPI));
        } else {
            gsl_spmatrix_set(covmatrix, elem.i, elem.ip, 4.0*permutationCoef*( cos(xp*(a0-ad0)*twoPI) + cos(xp*(a0-ad1)*twoPI) ));
        }
    }
}

vecd calculateCovariancesForXZero(const Environement &env, const std::vector<BaseState> &B, gsl_vector *u, const std::vector<Elem> &offDiagonalNonzeroElems){
    vecd covariances(env.corrPoints+1);

    gsl_vector *v = gsl_vector_alloc (env.numberOfStates);
    gsl_spmatrix *covarianceMatrix = gsl_spmatrix_alloc_nzmax(env.numberOfStates, env.numberOfStates,offDiagonalNonzeroElems.size()+env.numberOfStates,GSL_SPMATRIX_TRIPLET);

    for(uint i=0; i<env.corrPoints+1; i++){
        generateCovarianceMatrix(env, B, covarianceMatrix,double(i)/double(env.corrPoints)/2.0,offDiagonalNonzeroElems);
        covariances[i] = inner(u,covarianceMatrix,v);
        printProgress("  Calculating G2: ",i,env.corrPoints+1);
    }

    gsl_vector_free(v);
    gsl_spmatrix_free(covarianceMatrix);

    return covariances;
}

//void calculateG1FunctionInBasisOfPlaneWaves(){
//    gsl_spmatrix *lambdaMatrix = gsl_spmatrix_alloc_nzmax(env.numberOfStates, env.numberOfStates,offDiagonalNonzeroElems.size()+env.numberOfStates,GSL_SPMATRIX_TRIPLET);
//    for(int j=0; j<env.numberOfStates;j++){
//        for(int jp=j; jp<env.numberOfStates;jp++){
//            lambdaMatrix[j,jp] = lambda(j, jp);
//            lambdaMatrix[jp,j] = lambdaMatrix[j,jp];
//        }
//    }

//}

//double lambda(int j, int jp){
//    double out = 0;
//    for(int ip=0; ip<NoS; ip++){
//        for(int i=0;i<NoS;i++){
//            out += sqrt(base[i][j]*base[ip][jp])*a[i]*a[ip];
//        }
//    }
//    return out;
//}

#endif // CORRELATIONS_G2_H
