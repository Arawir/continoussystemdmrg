#ifndef PHYSICS_H
#define PHYSICS_H

#include "commonMethods.h"

// Potentials //////////////////////////////////////////////////////////////////////////////////////////////

double vintDipole(const Environement &env, int k){
    double k2 = pow( twoPI*double(k)*env.sigma, 2 )/2.0;

    if(k2<1e-6){ return -env.gdd; }
    if(k2<100.0){ return -env.gdd*(1.0 + k2*exp(k2) * gsl_sf_expint_Ei(-k2)); }
    return  -env.gdd* (1.0 - (k2*k2 + 4.03640*k2 + 1.15198)/(k2*k2 + 5.03637*k2 + 4.19160) );
}

double vddsigma(double y, double sigma){
    return (2*abs(y/sigma)-sqrt(twoPI)*(1+y/sigma*y/sigma)*exp(y/sigma*y/sigma/2)*erfc(abs(y/sigma)/sqrt(2))) / 4 / sigma;
}

double vintDipoleMy(const Environement &env, int k){
    double sum = 0.0;
    int nPoints = 1000;
    double d = 1.0/double(nPoints);
    for(int j=0; j<nPoints; j++){
        double y =double(j)/double(nPoints);
        sum += cos(twoPI*double(k)*y) * vddsigma(y,env.sigma) + cos(twoPI*double(k)*(y+d)) * vddsigma((y+d),env.sigma);
    }
    return -env.gdd * sum*d;
}

double vintWellV(const Environement &env, int k){
    if(k==0){ return -env.vqw *env.aqw; }
    return -2.0*env.vqw*sin(PI*double(k)*env.aqw);
}

void vintLoad(const Environement &env, std::vector<double> &vintlist){
    std::ifstream file(env.potentialType.c_str());
    for(uint k=0; k<=2*env.pmax; k++){
        file >> vintlist[k];
        vintlist[k] += env.g;
    }
    file.close();
}


vecd prepareVIntList(const Environement &env){
    std::vector<double> vintlist(2*env.pmax+1,0.0);

    if(env.potentialType.find(".vks")!=std::string::npos){
        vintLoad(env,vintlist);
    } else {
        for(uint k=0; k<=2*env.pmax; k++){
            if(env.potentialType=="dipole"){
                vintlist[k]=vintDipole(env,k) + env.g;
            } else if(env.potentialType=="wellV"){
                vintlist[k]=vintWellV(env,k) + env.g;
            } else if(env.potentialType=="dipoleMy"){
                vintlist[k]=vintDipoleMy(env,k) + env.g;
            } else {
                std::cout << "ERROR: Unknown potential type!" << std::endl;
                assert(0);
            }
        }
    }
    return vintlist;
}

// Hamiltonian ////////////////////////////////////////////////////////////

void generateHamiltonianMatrix(int numberOfStates, const std::vector<BaseState> &B, const std::vector<double> &vintlist, int pmax, const std::vector<Elem> &offDiagonalNonzeroElems, gsl_spmatrix *H){
    for(int i=0;i<numberOfStates;i++){  //diagonal elements
        double element=0.;
        for(uint k=0; k<B[i].size(); k++){
            element += twoPIPI*B[i][k]*intpow((int)k-pmax, 2) + B[i][k]*(B[i][k]-1)*vintlist[0]/2.0;
            for(uint j=0; j<k; j++){
                element+=B[i][k]*B[i][j]*( vintlist[0] + vintlist[k-j]);
            }
        }
        gsl_spmatrix_set(H, i, i, element);
    }

    for(auto &elem : offDiagonalNonzeroElems){  //non-diagonal elements
        auto [permutationCoef, a0,a1,ad0,ad1] = getAsandAdds(elem.i, elem.ip, B);

        if( a0==a1 && ad0==ad1 ){
            gsl_spmatrix_set(H, elem.i, elem.ip, 2.0*permutationCoef * vintlist[abs(a0-ad0)] );
        } else if( a0==a1 && ad0!=ad1 ){
            gsl_spmatrix_set(H, elem.i, elem.ip, 2.0*permutationCoef * vintlist[abs(a0-ad0)] );
        } else if( a0!=a1 && ad0==ad1 ){
            gsl_spmatrix_set(H, elem.i, elem.ip, 2.0*permutationCoef * vintlist[abs(a0-ad0)] );
        } else {
            gsl_spmatrix_set(H, elem.i, elem.ip, 2.0*permutationCoef * ( vintlist[abs(a0-ad0)] + vintlist[abs(a0-ad1)]) );
        }
    }

    printDone("  Generate Hamiltonian Matrix ");
}

#endif // PHYSICS_H
