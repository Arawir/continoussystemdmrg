#include "commonMethods.h"
#include "correlations_g2.h"
#include "lanczos.h"
#include "physics.h"

void printEnergyAndVariation(const Environement &env, const vecd &varlist, const gsl_vector *eval, const double &energyOffset){
    for(uint i=env.m-1; i>=env.m-env.neig; i--){
        std::cout << "  State-> " << i
                  << "  Energy-> " << gsl_vector_get(eval,i)+energyOffset
                  << "  Energy variation (should be 0)-> "<< varlist[i-env.m+env.neig];
    }
    printDone(" ");
}

void saveEigenvectorsToFile(const Environement &env, const gsl_matrix *evec){
    std::ofstream file("data/vectors_"+env.name+".txt");

    gsl_vector *vloc = gsl_vector_alloc (env.numberOfStates);
    for(int i=env.neig-1; i>=0; i--){
        gsl_matrix_get_col(vloc,evec,i);
        for(uint j=0; j<env.numberOfStates; j++){
            file << std::fixed << std::setprecision(15)
              << gsl_vector_get(vloc,j) << "\t";
        }
        file << std::endl;
    }
    gsl_vector_free(vloc);

    file.close();
}

void saveEnergiesToFile(const Environement &env, const vecd &varlist, const double &energyOffset, const gsl_vector *eval){
    std::ofstream file( "data/energies_"+env.name+".txt");

    for(uint i=env.m-1; i>=env.m-env.neig; i--){
        file << std::fixed << std::setprecision(15)
          << gsl_vector_get(eval,i)+energyOffset <<"\t"
          << varlist[i-env.m+env.neig]<<std::endl;
    }

    file.close();
}

void saveCovariancesToFile(const Environement &env, const std::vector<double> &cov){
    std::ofstream file("data/cov_"+env.name+".txt");

    for(uint i=0; i<cov.size(); i++){
        file << std::setprecision(15)<<std::fixed
             << double(i)/double(env.corrPoints)/2.0
             << "\t" << cov[i] << std::endl;

        std::cout << "  " << double(i)/double(env.corrPoints)/2.0
                  << "\t" << cov[i] << std::endl;
    }

    file.close();
}

double findEnergyOffset(const Environement &env, gsl_spmatrix *H){
    int m0=10;
    gsl_vector *eval0 = gsl_vector_alloc (m0);
    gsl_matrix *evec0 = gsl_matrix_alloc (env.numberOfStates, 1);
    lanczos(1,env.numberOfStates,m0,H,evec0,eval0);
    double energyOffset=gsl_vector_get(eval0,m0-1);
    gsl_vector_free(eval0);
    gsl_matrix_free(evec0);
    return energyOffset;
}

void prepareHamiltonianWithOffsetMatrix(const Environement &env, double &energyOffset, gsl_spmatrix *H){
    for(uint i=0; i<env.numberOfStates; i++){
        double tmp = gsl_spmatrix_get(H,i,i) - energyOffset;
        gsl_spmatrix_set(H, i, i, tmp);
    }
}

/////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]){
    // Load data /////////////////////////////////////////////////////////////
    Environement env{argv};
    env.numberOfStates = calculateNumberOfStates(env);
    std::cout << env.numberOfStates << std::endl;


    // Prepare Hamiltonian /////////////////////////////////////////////////////////////
    std::vector<BaseState> baseStates(env.numberOfStates, BaseState(env.oneParticleStates, 0.0) );
    prepareMatrixB(env, baseStates);
//    for (auto& a : baseStates){
//        for(auto&b : a){
//            std::cout << b << " ";
//        }
//        std::cout << std::endl;
//    }
    std::vector<Elem> nonDiagnonzeroElems = generateListOfOffDiagNonzeroElements(env, baseStates);
    std::vector<double> vintlist = prepareVIntList(env);
    gsl_spmatrix *H = gsl_spmatrix_alloc_nzmax(env.numberOfStates, env.numberOfStates, nonDiagnonzeroElems.size()+env.numberOfStates+1, GSL_SPMATRIX_TRIPLET);
    generateHamiltonianMatrix(env.numberOfStates, baseStates, vintlist, env.pmax, nonDiagnonzeroElems, H);


    // Calculate offset /////////////////////////////////////////////////////////////
    double energyOffset = findEnergyOffset(env, H);
    prepareHamiltonianWithOffsetMatrix(env, energyOffset, H);
    printDone("  Prepare hamiltonian matrix with offset");


    // Lanczos /////////////////////////////////////////////////////////////
    gsl_vector *eval = gsl_vector_alloc (env.m);
    gsl_matrix *evec = gsl_matrix_alloc (env.numberOfStates, env.neig);
    lanczos(env.neig, env.numberOfStates, env.m, H, evec, eval);
    vecd varlist = var(env.neig, env.numberOfStates, evec, H);
    gsl_spmatrix_free(H);


    // Save data /////////////////////////////////////////////////////////////
    printEnergyAndVariation(env, varlist, eval, energyOffset);
    saveEigenvectorsToFile(env, evec);
    saveEnergiesToFile(env, varlist, energyOffset, eval);


    // Calculate G2 correlations /////////////////////////////////////////////////////
    if(env.corrPoints > 0){
        gsl_vector *u = gsl_vector_alloc (env.numberOfStates);
        gsl_matrix_get_col(u, evec, 0);
        std::vector<double> cov = calculateCovariancesForXZero(env, baseStates, u, nonDiagnonzeroElems);
        saveCovariancesToFile(env, cov);
        gsl_vector_free(u);
    }
    std::ofstream file("data/BECFraction_"+env.name+".txt");
    file << gsl_matrix_get(evec,0,0) << std::endl;
    file.close();
    gsl_vector_free(eval);
    gsl_matrix_free(evec);


    printf("Everytihng done in %.2f [s], peak vMemory %d [kB])\r\n", (double)(clock() - globalTimeStart)/CLOCKS_PER_SEC, process_stat("VmPeak:"));
    return 0;
}
