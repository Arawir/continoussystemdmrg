#ifndef COMMONMETHODS_H
#define COMMONMETHODS_H

#include <cassert>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <vector>

#include <gsl/gsl_spblas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_expint.h>

#define PI 3.14159265359
#define twoPI 6.283185307
#define twoPIPI 19.739208802

typedef std::vector<double> vecd;
typedef unsigned int uint;
typedef std::vector<int> BaseState;


// Numerical methods ///////////////////////////////////////////////////////////

struct Elem{
  unsigned int i, ip;
  Elem(unsigned int _i, unsigned int _ip){ i = _i; ip = _ip; }
};

struct Environement{
    double sigma=0, g=0, gdd=0, vqw=0, aqw=0;
    std::string potentialType = "";
    std::string name="";
    bool TGRegime=false;

    uint N, pmax, numberOfStates, neig, m, corrPoints, oneParticleStates;
    int totalMomentum, cutoff;

    Environement(char *argv[]){
        std::istringstream(argv[1])>>N;
        std::istringstream(argv[2])>>pmax;
        std::istringstream(argv[3])>>totalMomentum;
        std::istringstream(argv[4])>>neig;
        std::istringstream(argv[5])>>m;
        std::istringstream(argv[6])>>corrPoints;
        std::istringstream(argv[7]) >> potentialType;

        oneParticleStates=2*pmax+1;
        cutoff=pmax*pmax;
        name = "n"+std::to_string(N)+"_pmax"+std::to_string(pmax)+"_ptot"+std::to_string(totalMomentum)+"_neig"+std::to_string(neig)+"_m"+std::to_string(m);


        if(potentialType == "dipole"){
            if(std::string(argv[8])=="inf"){
                TGRegime = true;
            }else {
                std::istringstream(argv[8])>>g;
            }
            std::istringstream(argv[9])>>gdd;
            std::istringstream(argv[10])>>sigma;
            name += "_g"+std::to_string(g)+"_gdd"+std::to_string(gdd)+"_sigma"+std::to_string(sigma);
        } else if(potentialType == "dipoleMy"){
            if(std::string(argv[8])=="inf"){
                TGRegime = true;
            }else {
                std::istringstream(argv[8])>>g;
            }
            std::istringstream(argv[9])>>gdd;
            std::istringstream(argv[10])>>sigma;
            name += "_g"+std::to_string(g)+"_gdd"+std::to_string(gdd)+"_sigma"+std::to_string(sigma);
        } else if(potentialType == "wellV"){
            if(std::string(argv[8])=="inf"){
                TGRegime = true;
            }else {
                std::istringstream(argv[8])>>g;
            }
            std::istringstream(argv[9])>>vqw;
            std::istringstream(argv[10])>>aqw;
            name += "_g"+std::to_string(g)+"_vqw"+std::to_string(vqw)+"_aqw"+std::to_string(aqw);
        } else{
            std::cout << "ERROR: unknown potential type!" << std::endl;
            assert(0);
        }
    }
};

inline int intpow(int a, int b){
     int c=a;
     for (int n=b; n>1; n--) c*=a;
     return c;
}

inline double inner(const gsl_vector *ket, const gsl_spmatrix *hermitianMatrix, gsl_vector *bra){
    double out=-123.0;
    gsl_spblas_dgemv(CblasNoTrans,0.5,hermitianMatrix,ket,0.0, bra);
    gsl_spblas_dgemv(CblasTrans,0.5,hermitianMatrix,ket,1.0,bra);
    gsl_blas_ddot(bra,ket, &out );
    return out;
}

////////////////////////////////////////////////////////////////////////////////////////////
inline int process_stat(std::string paramName){ // "VmSize:" or "VmPeak:"
    std::ifstream stat_stream("/proc/self/status",std::ios_base::in);
    int out;
    std::string tmp = "";
    while(tmp != paramName){
       stat_stream >> tmp;
    }
    stat_stream >> out;
    stat_stream.close();
    return out;
}

clock_t globalTimeStart{clock()};
clock_t globalTimeLast{clock()};

void printDone(std::string text){
  printf("%s (DONE %.2f [s], %d [kB])\r\n",text.c_str(), (double)(clock() - globalTimeLast)/CLOCKS_PER_SEC, process_stat("VmSize:"));
  globalTimeLast=clock();
}
void printDone(std::string text, int i){
  printf("%s %d / %d (DONE %.2f [s], %d [kB])\r\n",text.c_str(), i,i,(double)(clock() - globalTimeLast)/CLOCKS_PER_SEC, process_stat("VmSize:"));
  globalTimeLast=clock();
}

inline void printProgress(std::string text, int n, int max){
    if(n+1==max){
        printDone(text, max);
    } else {
        if(max > 50){
            if(n%(max/50)==0){
                std::cout<<text << n+1 <<" / "<< max << "\r";
                std::cout.flush();
            }
        } else {
            std::cout<<text << n+1 <<" / "<< max << "\r";
            std::cout.flush();
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
inline int calculateEnergyOfState( const std::vector<uint> &state){
    int energy=0;
    for(unsigned int i=1; i<state.size(); i++){
        energy += intpow((state[i]+1)/2, 2);
    }
    return energy;
}

inline int calculateMomentumOfState( const std::vector<uint> &state){
    int momentum=0;
    for(unsigned int i=1; i<state.size(); i++){
        momentum += ((state[i]+1)/2)*intpow(-1,state[i]);
    }
    return momentum;
}

inline bool isTG(const std::vector<uint> &state){
    for(uint i=0; i<state.size()-1; i++){
        if(state[i]==state[i+1]) return false;
    }
    return true;
}

int calculateNumberOfStates(const Environement &env){
    int numberOfStates=0;
    std::vector<uint> state(env.N+1,0);
    state[0]=env.oneParticleStates-1;

    if(env.totalMomentum==0){ numberOfStates++; } //wektor [0 0 ... 0] nie występuje w procedurze

    while(state[1]<2*env.pmax){
        int li;
        for(li=env.N; li>0; li--){
            if(state[li]<state[li-1]){
                state[li]++;
                break;
            } else { state[li]=0; }
        }

        while( calculateEnergyOfState(state)>env.cutoff ){
            while( li>1 && state[li]==state[li-1] ){
                state[li]=0;
                li--;
            }
            state[li]++;
        }
        if( calculateMomentumOfState(state)==env.totalMomentum ){
            if( (env.TGRegime && isTG(state)) || !env.TGRegime ){
                numberOfStates++;
            }
        }
    }
    return numberOfStates;
}

inline void convertStateFromConventionWojtekToStandard(const Environement &env, const std::vector<uint> &state, const uint stateIndex, std::vector<BaseState>& baseStates){
    for(uint i=1;i<=env.N;i++){
        int iped=((state[i]+1)/2)*intpow(-1,state[i]);
        baseStates[stateIndex][env.pmax+iped]+=1;
    }
}

void prepareMatrixB(const Environement &env, std::vector<BaseState>& baseStates ){
    std::vector<uint> state(env.N+1,0);
    state[0]=env.oneParticleStates-1;

    uint stateIndex=0;
    if(env.totalMomentum==0){
        for(uint i=1;i<=env.N;i++){
            int iped=((state[i]+1)/2)*intpow(-1,state[i]);
            baseStates[stateIndex][env.pmax+iped]+=1;
        }
        stateIndex++;
    }

    while(state[1]<2*env.pmax){
        int li;
        for(li=env.N;li>0;li--){
            if(state[li]<state[li-1]){
                state[li]++;
                break;
            } else {
                state[li]=0;
            }
        }

        while( calculateEnergyOfState(state)>env.cutoff ){
            while(li>1&&state[li]==state[li-1]){
                state[li]=0;
                li--;
            }
            state[li]++;
        }

        if( calculateMomentumOfState(state) == env.totalMomentum ){
            if( (env.TGRegime && isTG(state)) || !env.TGRegime ){
                convertStateFromConventionWojtekToStandard(env, state, stateIndex, baseStates);
                stateIndex++;
            }
        }
    }
}


std::vector<Elem> generateListOfOffDiagNonzeroElements(const Environement &env, const std::vector<BaseState> &B){
    std::vector<Elem> offDiagNonzeroElems;

    for(uint i=0; i<env.numberOfStates; i++){
        for(uint ip=0; ip<i; ip++){//hamiltonian matrix is hermitian, ip<i
            uint differences=0;
            for(uint iter=0; iter<env.oneParticleStates; iter++){
                differences+=abs(B[i][iter]-B[ip][iter]);
                if( differences > 4 ) break;
            }
            if(differences==4){ offDiagNonzeroElems.push_back( Elem{i,ip} ); }
        }
        printProgress("  Preparing list of nonzero elements: ",i,env.numberOfStates);
    }

    return offDiagNonzeroElems;
}

inline std::tuple<double, int, int,int,int> getAsandAdds(int i, int ip, const std::vector<BaseState> &B){
    int ai=0, adi=0;
    int a[2], ad[2];
    for(uint k=0;k<B[i].size();k++){
        if(B[i][k]-B[ip][k]==1){
            a[ai]=k;
            ai++;
        } else if(B[i][k]-B[ip][k]==2){
            a[0]=k;
            a[1]=k;
        } else if(B[i][k]-B[ip][k]==-1){
            ad[adi]=k;
            adi++;
        } else if(B[i][k]-B[ip][k]==-2){
            ad[0]=k;
            ad[1]=k;
        }
    }
    double permutationCoef;
    if( a[0]==a[1] && ad[0]==ad[1] ){
        permutationCoef=1./2*sqrt(B[i][a[0]]*(B[i][a[0]]-1)*B[ip][ad[0]]*(B[ip][ad[0]]-1));
    } else if( a[0]==a[1] && ad[0]!=ad[1] ){
        permutationCoef = sqrt(B[i][a[0]]*(B[i][a[0]]-1)*B[ip][ad[0]]*B[ip][ad[1]]);
    } else if( a[0]!=a[1] && ad[0]==ad[1] ){
        permutationCoef = sqrt(B[i][a[0]]*B[i][a[1]]*B[ip][ad[0]]*(B[ip][ad[0]]-1));
    } else {
        permutationCoef = sqrt(B[i][a[0]]*B[i][a[1]]*B[ip][ad[0]]*B[ip][ad[1]]);
    }
    return  {permutationCoef, a[0],a[1],ad[0],ad[1]};
}

#endif // COMMONMETHODS_H
