#include "itensor/all.h"
#include <string>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cassert>

using namespace itensor;

int N =0;
int Ns = 0;
int Nn = 0;
double omega = 0.0;
double g = 0.0;
double D = 0.0;
double maxlambda = 0;
int PBC = 0;


std::vector<std::vector<double>> K;
std::vector<std::vector<double>> Ups0;
std::vector<std::vector<double>> Upsp1;
std::vector<std::vector<double>> Upsm1;

std::vector<std::vector<std::vector<std::vector<double>>>> U;


void loadElems(){
    std::ifstream file ("../../elems/K_Ns2_Nn3");
    K.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        K[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> K[n][np];
        }
    }
    file.close();

    file.open("../../elems/Ups0_Ns2_Nn3");
    Ups0.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        Ups0[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> Ups0[n][np];
        }
    }
    file.close();

    file.open("../../elems/Upsp1_Ns2_Nn3");
    Upsp1.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        Upsp1[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> Upsp1[n][np];
        }
    }
    file.close();

    file.open("../../elems/Upsm1_Ns2_Nn3");
    Upsm1.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        Upsm1[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> Upsm1[n][np];
        }
    }

    file.close();

    file.open("../../elems/U_Ns2_Nn3");
    U.resize(Nn);
    for (int i = 0; i < Nn; ++i){
        U[i].resize(Nn);
        for (int ip = 0; ip < Nn; ++ip){
            U[i][ip].resize(Nn);
            for (int ipp = 0; ipp < Nn; ++ipp){
                U[i][ip][ipp].resize(Nn);
            }
        }
    }

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            for(int npp=0;npp<Nn;npp++){
                for(int nppp=0;nppp<Nn;nppp++){
                    double tmp =0;
                    file >> tmp;
                    file >> tmp;
                    file >> tmp;
                    file >> tmp;
                    file >> U[n][np][npp][nppp];
                }
            }
        }
    }

    file.close();
}

int ind(int j,int n){
    return j*Nn+n+Nn*Ns/2+1;
}
double myabs(double x){
    if (x<0){ return -x; }
    return x;
}

MPO generateH(double Lambda, Boson &sites){
    auto ampo = AutoMPO(sites);

    for (int j=-Ns/2;j<Ns/2;j++){
        for(int n=0; n<Nn;n++){
            for(int np=0;np<Nn;np++){
                ampo += K[n][np]+Lambda*Ups0[n][np],"Adag",ind(j,np),"A",ind(j,n);
                if( j<int(Ns/2)-1){
                    ampo += Lambda*Upsp1[n][np],"Adag",ind(j+1,np),"A",ind(j,n);
                    ampo += Lambda*Upsm1[n][np],"Adag",ind(j,np),"A",ind(j+1,n);
                }
                if (myabs(g)>1e-6){
                    for (int npp=0; npp<Nn; npp++){
                        for (int nppp=0; nppp<Nn; nppp++){
                            if ( myabs(U[n][np][npp][nppp])>1e-6){
                                ampo += g*U[n][np][npp][nppp],"Adag",ind(j,n),"Adag",ind(j,np),"A",ind(j,npp),"A",ind(j,nppp);
                               // H += g*U[n,np,npp,nppp]*( ((hb[j,n].T).dot(hb[j,np].T)).dot((hb[j,npp]).dot(hb[j,nppp])) )
                            }
                        }
                    }
                }
            }
        }
    }

    return toMPO(ampo);
}

MPO generateHK(Boson &sites){
    auto ampo = AutoMPO(sites);

    for (int j=-Ns/2;j<Ns/2;j++){
        for(int n=0; n<Nn;n++){
            for(int np=0;np<Nn;np++){
                ampo += K[n][np],"Adag",ind(j,np),"A",ind(j,n);
            }
        }
    }

    return toMPO(ampo);
}

MPO generateHU(Boson &sites){
    auto ampo = AutoMPO(sites);

    for (int j=-Ns/2;j<Ns/2;j++){
        for(int n=0; n<Nn;n++){
            for(int np=0;np<Nn;np++){
                for (int npp=0; npp<Nn; npp++){
                    for (int nppp=0; nppp<Nn; nppp++){
                        if ( myabs(U[n][np][npp][nppp])>1e-6){
                            ampo += g*U[n][np][npp][nppp],"Adag",ind(j,n),"Adag",ind(j,np),"A",ind(j,npp),"A",ind(j,nppp);
                        }
                    }
                }
            }
        }
    }

    return toMPO(ampo);
}

void printHamiltonian(Boson &sites,MPO& H){

    auto stateL = InitState(sites,"0");
    auto stateR = InitState(sites,"0");
    MPS BSL =  MPS(stateL);
    MPS BSR =  MPS(stateR);
    int bl=0;
    int br=0;

    for(int n1=0; n1<Ns*Nn; n1++){
        for(int n2=n1;n2<Ns*Nn;n2++){
            br=0;
            for(int n1p=0; n1p<Ns*Nn; n1p++){
                for(int n2p=n1p;n2p<Ns*Nn;n2p++){
                    stateL = InitState(sites,"0");
                    stateR = InitState(sites,"0");
                    if(n1==n2){
                        stateL.set(n1+1,"2");
                    } else{
                        stateL.set(n1+1,"1");
                        stateL.set(n2+1,"1");
                    }

                    if(n1p==n2p){
                        stateR.set(n1p+1,"2");
                    } else{
                        stateR.set(n1p+1,"1");
                        stateR.set(n2p+1,"1");
                    }

                    BSL =  MPS(stateL);
                    BSR =  MPS(stateR);
                    if (myabs(inner(BSL,H,BSR))>1e-6){
                        std::cout << "("<<bl<<"," << br << ")  " << inner(BSL,H,BSR) << std::endl;
                    }
                    br++;
                }
            }
            bl++;
        }
    }
}


int main(int argc, char *argv[]){
    N = atoi(argv[1]);
    Ns = atoi(argv[2]);
    Nn = atoi(argv[3]);
    PBC = atoi(argv[4]);
    omega = atof(argv[5]);
    g = atof(argv[6]);
    maxlambda = atof(argv[7]);
    int maxOcc = atoi(argv[8]);

    D = 1.0/Ns;

//std::cout << argv[0]<< " " << atof(argv[1])*2.3 << std::endl;
    loadElems();
    Boson sites = Boson(Ns*Nn,{"MaxOcc=",maxOcc});

    auto HK = generateHK(sites);
    auto HU = generateHU(sites);


    auto H = generateH(0,sites);
    printHamiltonian(sites,H);

    std::cout << "DONE"<< std::endl;
    return 0;
}
