#include "itensor/all.h"
#include <string>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cassert>

using namespace itensor;

int N =0;
int Ns = 0;
int Nn = 0;
int maxRange=8;
double sigma=0.1;
double omega = 0.0;
double g = 0.0;
double gdd = 0.0;
double D = 0.0;
int PBC = 0;
int maxOcc = 2;


std::vector<std::vector<double>> K;
std::vector<std::vector<double>> UpsL0;
std::vector<std::vector<double>> UpsLp1;
std::vector<std::vector<double>> UpsLm1;

std::vector<std::vector<std::vector<std::vector<double>>>> U;
std::vector<std::vector<std::vector<std::vector<std::vector<double>>>>> Udd;


void loadElems(){
    std::cout << "Loading Hamiltonian elems from: " << std::endl;
    char str[1024];
    sprintf(str, "../../elems/K_Ns%i_Nn%i", Ns,Nn);
    std::cout << str << std::endl;

    std::ifstream file (str);
    K.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        K[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> K[n][np];
        }
    }
    file.close();

    sprintf(str, "../../elems/UpsL0_Ns%i_Nn%i", Ns,Nn);
    std::cout << str << std::endl;
    file.open(str);
    UpsL0.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        UpsL0[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> UpsL0[n][np];
        }
    }
    file.close();

    sprintf(str, "../../elems/UpsLp1_Ns%i_Nn%i", Ns,Nn);
    std::cout << str << std::endl;
    file.open(str);
    UpsLp1.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        UpsLp1[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> UpsLp1[n][np];
            std::cout << UpsLp1[n][np]  << std::endl;
        }
    }
    file.close();

    sprintf(str, "../../elems/UpsLm1_Ns%i_Nn%i", Ns,Nn);
    std::cout << str << std::endl;
    file.open(str);
    UpsLm1.resize(Nn);
    for (int i = 0; i < Nn; ++i)
        UpsLm1[i].resize(Nn);

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            double tmp =0;
            file >> tmp;
            file >> tmp;
            file >> UpsLm1[n][np];
        }
    }

    file.close();

    sprintf(str, "../../elems/U_Ns%i_Nn%i", Ns,Nn);
    std::cout << str << std::endl;
    file.open(str);
    U.resize(Nn);
    for (int i = 0; i < Nn; ++i){
        U[i].resize(Nn);
        for (int ip = 0; ip < Nn; ++ip){
            U[i][ip].resize(Nn);
            for (int ipp = 0; ipp < Nn; ++ipp){
                U[i][ip][ipp].resize(Nn);
            }
        }
    }

    for(int n=0;n<Nn;n++){
        for(int np=0;np<Nn;np++){
            for(int npp=0;npp<Nn;npp++){
                for(int nppp=0;nppp<Nn;nppp++){
                    double tmp =0;
                    file >> tmp;
                    file >> tmp;
                    file >> tmp;
                    file >> tmp;
                    file >> U[n][np][npp][nppp];
                }
            }
        }
    }

    file.close();

    sprintf(str, "../../elems/Udd_Ns%i_Nn%i_sigma%.3f", Ns,Nn,sigma);
    std::cout << str << std::endl;
    file.open(str);
    Udd.resize(maxRange);
    for(int j=0;j<maxRange;j++){
        Udd[j].resize(Nn);
        for (int i = 0; i < Nn; ++i){
            Udd[j][i].resize(Nn);
            for (int ip = 0; ip < Nn; ++ip){
                Udd[j][i][ip].resize(Nn);
                for (int ipp = 0; ipp < Nn; ++ipp){
                    Udd[j][i][ip][ipp].resize(Nn);
                }
            }
        }
    }
    for(int j=0;j<maxRange;j++){
        for(int n=0;n<Nn;n++){
            for(int np=0;np<Nn;np++){
                for(int npp=np;npp<Nn;npp++){
                    for(int nppp=n;nppp<Nn;nppp++){
                        double tmp =0;
                        file >> tmp;
                        file >> tmp;
                        file >> tmp;
                        file >> tmp;
                        file >> tmp;
                        file >> Udd[j][n][np][npp][nppp];
                        std::cout << j << " " << n << " " << np << " "<< npp << " "<< nppp << " "<<  Udd[j][n][np][npp][nppp] << std::endl;
                    }
                }
            }
        }
    }

    file.close();

}

int ind(int j,int n){
    return j*Nn+n+Nn*Ns/2+1;
}
double myabs(double x){
    if (x<0){ return -x; }
    return x;
}

MPO generateH(double Lambda, Boson &sites){
    auto ampo = AutoMPO(sites);

    for (int j=-Ns/2;j<Ns/2;j++){
        for(int n=0; n<Nn;n++){
            for(int np=0;np<Nn;np++){
                ampo += 0.5*K[n][np]+Lambda*UpsL0[n][np],"Adag",ind(j,np),"A",ind(j,n);
                if( j<int(Ns/2)-1){
                    ampo += Lambda*UpsLp1[n][np],"Adag",ind(j+1,np),"A",ind(j,n);
                    ampo += Lambda*UpsLm1[n][np],"Adag",ind(j,np),"A",ind(j+1,n);
                } else{
                    if(PBC==1){
                        ampo += Lambda*UpsLp1[n][np],"Adag",ind(-Ns/2,np),"A",ind(j,n);
                        ampo += Lambda*UpsLm1[n][np],"Adag",ind(j,np),"A",ind(-Ns/2,n);
                    }
                }
                if (myabs(g)>1e-6){
                    for (int npp=0; npp<Nn; npp++){
                        for (int nppp=0; nppp<Nn; nppp++){
                            if ( myabs(U[n][np][npp][nppp])>1e-6){
                                if(((n==np)||(npp==nppp))&&(maxOcc<2) ){ continue; }
                                ampo += g/2*U[n][np][npp][nppp],"Adag",ind(j,n),"Adag",ind(j,np),"A",ind(j,npp),"A",ind(j,nppp);
                            }
                        }
                    }
                }
                if (myabs(gdd)>1e-6){
                    for (int npp=0; npp<Nn; npp++){
                        for (int nppp=0; nppp<Nn; nppp++){
                            for(int jp=j;jp<=j+maxRange-1;jp++){
                                double coef = Udd[jp-j][std::min(n,nppp)][std::min(np,npp)][std::max(np,npp)][std::max(n,nppp)];
                                if( myabs(coef)>1e-6){
                                    if( jp>=Ns/2){
                                        if(PBC==0){
                                            continue;
                                        } else {
                                            ampo += gdd/2*coef,"Adag",ind(j,n),"Adag",ind(jp-Ns,np),"A",ind(jp-Ns,npp),"A",ind(j,nppp);
                                        }
                                    } else {
                                        ampo += gdd/2*coef,"Adag",ind(j,n),"Adag",ind(jp,np),"A",ind(jp,npp),"A",ind(j,nppp);
                                    }
                                }

                            }
                        }
                    }
                }

            }
        }
    }

    return toMPO(ampo);
}

MPO generateHK(Boson &sites){
    auto ampo = AutoMPO(sites);

    for (int j=-Ns/2;j<Ns/2;j++){
        for(int n=0; n<Nn;n++){
            for(int np=0;np<Nn;np++){
                ampo += 0.5*K[n][np],"Adag",ind(j,np),"A",ind(j,n);
            }
        }
    }

    return toMPO(ampo);
}

MPO generateHU(Boson &sites){
    auto ampo = AutoMPO(sites);

    for (int j=-Ns/2;j<Ns/2;j++){
        for(int n=0; n<Nn;n++){
            for(int np=0;np<Nn;np++){
                for (int npp=0; npp<Nn; npp++){
                    for (int nppp=0; nppp<Nn; nppp++){
                        if ( myabs(U[n][np][npp][nppp])>1e-6){
                            if(((n==np)||(npp==nppp))&&(maxOcc<2) ){ continue; }
                            ampo += g/2*U[n][np][npp][nppp],"Adag",ind(j,n),"Adag",ind(j,np),"A",ind(j,npp),"A",ind(j,nppp);
                        }
                    }
                }
            }
        }
    }

    return toMPO(ampo);
}

MPO generateHUdd(Boson &sites){
    auto ampo = AutoMPO(sites);

    for (int j=-Ns/2;j<Ns/2;j++){
        for(int n=0; n<Nn;n++){
            for(int np=0;np<Nn;np++){
                for (int npp=0; npp<Nn; npp++){
                    for (int nppp=0; nppp<Nn; nppp++){
                        for(int jp=j;jp<=j+maxRange-1;jp++){
                            double coef = Udd[jp-j][std::min(n,nppp)][std::min(np,npp)][std::max(np,npp)][std::max(n,nppp)];
                            if( myabs(coef)>1e-6){
                                if( jp>=Ns/2){
                                    if(PBC==0){
                                        continue;
                                    } else {
                                        ampo += gdd/2*coef,"Adag",ind(j,n),"Adag",ind(jp-Ns,np),"A",ind(jp-Ns,npp),"A",ind(j,nppp);
                                    }
                                } else {
                                    ampo += gdd/2*coef,"Adag",ind(j,n),"Adag",ind(jp,np),"A",ind(jp,npp),"A",ind(j,nppp);
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    return toMPO(ampo);
}



void step(double lambda, MPS &psi, Boson &sites, MPO &HK, MPO &HU, MPO &HUdd, Sweeps &sweeps){
    std::cout << "NEW STAGE -> Lambda=" << lambda << std::endl;
    std::time_t time0 = std::time(nullptr);

    auto H = generateH(lambda,sites);
    double energy0 = inner(psi,H,psi);
    double energy = dmrg(psi,H,sweeps,{"Quiet=",true});

    for(int i=0; i<20; i++){
        if((energy0-energy)<1e-7){
            break;
        }
        energy0 = energy;
        energy = dmrg(psi,H,sweeps,{"Quiet=",true});
    }

    auto C = correlationMatrix(psi,sites,"Adag","A");

    char str[1024];
    if(maxOcc<2){
        sprintf(str, "N%i_Ns%i_Nn%i_PBC%i_Lambda%.2f_omega%.6f_ginf_gdd%.6f_sigma%.6f_maxRange%i_maxOcc%i_DMRG", N,Ns,Nn,PBC,lambda,omega,gdd,sigma,maxRange,maxOcc);
    } else {
        sprintf(str, "N%i_Ns%i_Nn%i_PBC%i_Lambda%.2f_omega%.6f_g%.6f_gdd%.6f_sigma%.6f_maxRange%i_maxOcc%i_DMRG", N,Ns,Nn,PBC,lambda,omega,g,gdd,sigma,maxRange,maxOcc);
    }
    std::cout << str << std::endl;

    if(lambda>500.0){
        std::ofstream fileCorrs1 (std::string("../../Corrs1/")+str);
        std::ofstream fileCorrs2 (std::string("../../Corrs2/")+str);

        for(int j=-Ns/2; j<Ns/2;j++){
            for(int jp=-Ns/2; jp<Ns/2;jp++){
                for(int n=0; n<Nn; n++){
                    for(int np=0; np<Nn; np++){
                        fileCorrs1 << j << "  " << n << "  " << jp << "  " << np <<"  "<< C[ind(j,n)-1][ind(jp,np)-1] << std::endl;
                        for (int npp=0; npp<Nn; npp++){
                            for (int nppp=0; nppp<Nn; nppp++){
                                if(((n==np)||(npp==nppp))&&(maxOcc<2) ){
                                    fileCorrs2 << j << "  "<< jp << "  " << n << "  " << np <<"  "<< npp <<"  "<< nppp <<"  "<< 0.0 << std::endl;
                                } else {
                                    auto ampo = AutoMPO(sites);
                                    ampo += 1,"Adag",ind(j,n),"Adag",ind(jp,np),"A",ind(jp,npp),"A",ind(j,nppp);
                                    auto oper = toMPO(ampo);
                                    fileCorrs2 << j << "  "<< jp << "  " << n << "  " << np <<"  "<< npp <<"  "<< nppp <<"  "<< inner(psi,oper,psi)<< std::endl;
                                }
                            }
                        }
                    }
                }
            }
        }
        fileCorrs1.close();
        fileCorrs2.close();
    }

    std::ofstream fileEnergies (std::string("../../Energies/")+str);

    double EK = inner(psi,HK,psi);
    double EU = inner(psi,HU,psi);
    double EUdd = inner(psi,HUdd,psi);
    double EL = energy-EK-EU-EUdd;

    fileEnergies << "Kinetic_energy= " << EK<< std::endl;
    fileEnergies << "Short-range_interactions_energy= " << EU << std::endl;
    fileEnergies << "Dipole-dipole_interactions_energy= " << EUdd << std::endl;
    fileEnergies << "Disconitnuity_energy= " << EL << std::endl;
    fileEnergies << "Total_energy= " << energy << std::endl;


    fileEnergies.close();

    std::cout << "STAGE FINISHED -> EK,EU,EL=" << EK << ", " << EU << ", " << EL << " in " << std::time(nullptr)-time0 << " [s]" << std::endl;
}

int main(int argc, char *argv[]){
    ///////////////////////////////////////////////////////////////////////////
    ///// Get params //////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    std::time_t time0 = std::time(nullptr);
    N = atoi(argv[1]);
    Ns = atoi(argv[2]);
    Nn = atoi(argv[3]);
    PBC = atoi(argv[4]);
    omega = atof(argv[5]);
    g = atof(argv[6]);
    gdd = atof(argv[7]);
    sigma = atof(argv[8]);
    maxRange = atoi(argv[9]);
    maxOcc = atoi(argv[10]);

    D = 1.0/Ns;

    std::cout << "Run params:" << " N=" << N<< " Ns=" << Ns<< " Nn=" << Nn
              << " PBC=" << PBC<< " omega=" << omega<< " g=" << g<< " gdd=" << gdd
              << " sigma=" << sigma<< " maxRange=" << maxRange<< " maxOcc=" << maxOcc
              << std::endl;

    loadElems();


    ///////////////////////////////////////////////////////////////////////////
    ///// Prepare system //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    Boson sites = Boson(Ns*Nn,{"MaxOcc=",maxOcc});

    auto state = InitState(sites,"0");
    for(int i=0; i<N; i++){ state.set(int((i+0.5)*Ns*Nn/(N+1)),"1"); }
    MPS psi =  MPS(state);

    ///////////////////////////////////////////////////////////////////////////
    ///// Generate Observables and Sweeps /////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    auto HK = generateHK(sites);
    auto HU = generateHU(sites);
    auto HUdd = generateHUdd(sites);

    auto sweeps = Sweeps(4);
    sweeps.maxdim() = 1000;
    sweeps.mindim() = 10;
    sweeps.cutoff() = 1e-12;
    sweeps.niter() = 20;
    sweeps.noise() = 0;

    ///////////////////////////////////////////////////////////////////////////
    ///// Compute GS //////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    step(1.0, psi, sites, HK, HU, HUdd, sweeps);
    step(3.0, psi, sites, HK, HU, HUdd, sweeps);
    step(10.0, psi, sites, HK, HU, HUdd, sweeps);
    step(30.0, psi, sites, HK, HU, HUdd, sweeps);
    step(100.0, psi, sites, HK, HU, HUdd, sweeps);
    step(300.0, psi, sites, HK, HU, HUdd, sweeps);
    step(1000.0, psi, sites, HK, HU, HUdd, sweeps);
    step(3000.0, psi, sites, HK, HU, HUdd, sweeps);
    step(10000.0, psi, sites, HK, HU, HUdd, sweeps);

    std::cout << "DONE in " << std::time(nullptr)-time0 << " [s]"<< std::endl;
    return 0;
}
